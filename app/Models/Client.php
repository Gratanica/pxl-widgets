<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $table = 'tblclients';

    protected $guarded = [];

    function getCreditCard()
    {
        return $this->hasMany('App\Models\CreditCard');
    }

}
