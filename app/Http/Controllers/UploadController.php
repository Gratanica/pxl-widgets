<?php

namespace App\Http\Controllers;

use App\Http\Helpers\DataUploader;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    /**
     * @param Request $req
     * @return string
     */
    function index(Request $req)
    {
        try
        {
            $upload = $req->file('file');

            //check if a file is loaded, usually redirecting to this page.
            if(is_null($upload))
            {
                return view('pages/upload', ['message'=>null,'result'=>'no upload']);
            }

            //capture the file data
            $file = file_get_contents($upload);

            $content = [
                'file'              => $file,
                'filterAge'         => [
                    'min' => $req->txtAgeMin,
                    'max' => $req->txtAgeMax,
                ],
                'filterCCNumbers'   => $req->ccFilter == 'true' ? true : false
            ];

            $data = new DataUploader($content);

            $insert_count = $data->upload();

            $message = "Successfully Uploaded {$insert_count} records.";

            if($insert_count == 0)
            {
                $message = "Conditions do not match any data, no imports made.";
            }

            $result = true;
            return view('pages/upload', compact('message', 'result'));

        }
        catch(RequestException $requestException)
        {
            return view('pages/upload', ['message'=>"Request: {$requestException->getMessage()}", 'result'=>false]);

        }
        catch(\Exception $exception)
        {
            return view('pages/upload', ['message'=>"Exception: {$exception->getMessage()}", 'result'=>false]);
        }
        catch(\Error $error)
        {
            return view('pages/upload', ['message'=>"Error: {$error->getMessage()}", 'result'=>false]);
        }
    }
}
