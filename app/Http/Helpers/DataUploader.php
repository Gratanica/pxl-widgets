<?php

namespace App\Http\Helpers;

use App\Models\Client;
use App\Models\Creditcard;
use Carbon\Carbon;
use Illuminate\Support\Arr;

/**
 * Class DataUploader
 * @package App\Http\Helpers
 */
class DataUploader {

    /**
     * @var mixed
     */
    protected $file;

    /**
     * @var Formatter
     */
    protected $formatter;

    /**
     * @var mixed
     */
    protected $filter_age;

    /**
     * @var mixed
     */
    protected $filter_cc_numbers;

    /**
     * DataUploader constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->file = Arr::get($data, 'file');
        $this->filter_age = Arr::get($data, 'filterAge');
        $this->filter_cc_numbers = Arr::get($data, 'filterCCNumbers');
        $this->formatter = new Formatter($this->file);
    }

    /**
     * @return string
     */
    public function upload()
    {

        //converts a file into an array.
        $clients = $this->formatter->jsonToArray();

        //we set our counter
        $total_inserted = 0;

        foreach($clients as $client)
        {
            //should there not be any data in the file we exit.
            if(is_null($client))
            {
                return 0;
            }

            //some of the dates is not consistent, so we need to ensure it is
            $date = Arr::get($client,'date_of_birth');
            $date_of_birth = $this->formatter->prepareDate($date);

            $age_min = (int) Arr::get($this->filter_age, 'min', 0);
            $age_max = (int) Arr::get($this->filter_age, 'max', 0);

            //set conditions to true;
            $age_valid = true;
            $isConsecutive = true;

            //if no age filter set we dont check.
            if($age_min > 0 || $age_max > 0)
            {
                $age_valid = $this->isAgeBetween($date_of_birth, $age_min, $age_max);
            }

            //if age is not valid skip we skip client.
            if(!$age_valid)
            {
                continue;
            }

            if($this->filter_cc_numbers)
            {
                $credit_card_number = (int) Arr::get($client, 'credit_card.number');
                $isConsecutive = $this->isConsecutive($credit_card_number);
            }

            //if card number not consecutive we skip client.
            if(!$isConsecutive)
            {
                continue;
            }

            $data = [
                'name' => Arr::get($client,'name'),
                'address' => Arr::get($client,'address'),
                'checked' => Arr::get($client,'checked'),
                'description' => Arr::get($client,'description'),
                'interest' => Arr::get($client,'interest'),
                'date_of_birth' => $date_of_birth,
                'email' => Arr::get($client,'email'),
                'account' => Arr::get($client,'account')
            ];

            //insert the client information, but check if it exists first.
            $results = $this->addClient($data);

            //check if a row was inserted.
            if(!$results->wasRecentlyCreated)
            {
                continue;
            }

            $credit_card_information = Arr::get($client,'credit_card');

            //we now assume not everyone has a credit card, check credit card, else skip.
            if(!isset($credit_card_information))
            {
                continue;
            }

            $this->addCreditCard($results, $credit_card_information);
            $total_inserted++;
        }

        return $total_inserted;
    }

    /**
     * @param $data
     * @return mixed
     */
    private function addClient($data)
    {
        return Client::firstOrCreate($data, $data);
    }

    /**
     * @param $results
     * @param $credit_card_information
     */
    private function addCreditCard($results, $credit_card_information)
    {
        //insert clients credit card information.
        Creditcard::create(
            [
                'client_id' => $results->id,
                'type' => Arr::get($credit_card_information,'type'),
                'number' => Arr::get($credit_card_information,'number'),
                'name' => Arr::get($credit_card_information,'name'),
                'expirationDate' => Arr::get($credit_card_information,'expirationDate'),
            ]
        );
    }

    /**
     * @param $date_of_birth
     * @param $min
     * @param $max
     * @return bool
     */
    private function isAgeBetween($date_of_birth, $min, $max): bool
    {
        $age = Carbon::parse($date_of_birth)->age;

        if($age >= $min && $age <= $max)
        {
            return true;
        }

        return false;
    }

    /**
     * @param $cc_number
     * @return bool
     */
    private function isConsecutive($cc_number): bool
    {
        $num1 = (int) substr($cc_number,0,1);
        $num2 = (int) substr($cc_number,1,1);
        $num3 = (int) substr($cc_number,2,1);

        //check if all 3 numbers are the same.
        if( ($num1 == $num2) && ($num1 == $num3) )
        {
            return true;
        }

        return false;
    }
}
