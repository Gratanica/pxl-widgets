<?php

namespace App\Http\Helpers;

use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class Formatter {

    /**
     * @var
     */
    protected $file;

    /**
     * Formatter constructor.
     * @param $file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * @return mixed
     */
    public function jsonToArray()
    {
        return json_decode($this->file, true);
    }

    /**
     * @param $date
     * @return Carbon
     */
    public function prepareDate($date)
    {
        $format_date = Str::replace('/','-', $date);

        return Carbon::parse($format_date);
    }
}
