<div class="container">
    <hr>
    <div class="text-center center-block">
        <p class="txt-railway">- 2021 Grant Marang -</p>
        <br />
        <a href="mailto:grant@myview.co.za"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
        <a href="https://www.linkedin.com/in/grant-marang-7078ba181/"><i id="social-gp" class="fa fa-linkedin fa-3x social"></i></a>
    </div>
    <hr>
</div>
