@extends('layout.default')

@section('content')

    <div class="container">
        <div class="row">
            <section class="col-sm-6 col-md-12" id="main-content">
                <br />
                <h3 class="text-center mb-4">View Data Imported</h3>
                <hr class="my-4 shadow-lg" />
                <br />
                <div id="content_padded">
                    <div id="content">
                        <div class="table-responsive">
                            <table id="datatable" class="display compact dt-responsive nowrap">
                                <thead>
                                    <tr>
                                        <th>Client ID</th>
                                        <th>Name & Surname</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Checked</th>
                                        <th>Description</th>
                                        <th>Interest</th>
                                        <th>Date Of Birth</th>
                                        <th>Account</th>
                                        <th>Created</th>
                                        <th>Updated</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($clients as $client)
                                    <tr>
                                        <td>{{ $client->id }}</td>
                                        <td>{{ $client->name }}</td>
                                        <td>{{ $client->email }}</td>
                                        <td>{{ $client->address }}</td>
                                        <td>{{ $client->checked }}</td>
                                        <td>{{ $client->description }}</td>
                                        <td>{{ $client->interest }}</td>
                                        <td>{{ $client->date_of_birth }}</td>
                                        <td>{{ $client->account }}</td>
                                        <td>{{ $client->created_at }}</td>
                                        <td>{{ $client->updated_at }}</td>
                                    </tr>
                                @endforeach
                                <tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('javascript')
    <!-- datatable -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.2/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#datatable').DataTable();
        });
    </script>
@endsection


