@extends('layout.default')
@section('content')
    <div class="container mt-5" style="max-width: 500px">

        <div class="alert alert-warning mb-4 text-center">
            <h3 class="display-6">Upload Your JSON File</h3>
            <h5 class="display-6">(challenge.json)</h5>
        </div>

        <form id="fileUploadForm" method="POST" action="upload" enctype="multipart/form-data">
            @csrf
            <div class="form-group mb-3 text-center">
                <label class="form-control-label mb-2">Specify Age</label>
                <br>
                <input type="number" name="txtAgeMin" accept="application/JSON" class="form-control mb-1" placeholder="Minimum Age (not required)">
                <input type="number" name="txtAgeMax" accept="application/JSON" class="form-control" placeholder="Maximum Age (not required)">
            </div>

            <div class="form-group mb-3 text-center">
                <label class="form-control-label">Filter by 3 consecutive credit card numbers</label>
                <br>
                <input class="form-control-input" type="radio" name="ccFilter" value="true">
                <label class="form-control-label">
                    Yes
                </label>
                <input class="form-control-input" type="radio" name="ccFilter" value="false" checked>
                <label class="form-control-label">
                    No
                </label>
            </div>

            <div class="form-group mb-3">
                <input type="file" name="file" accept="application/JSON" class="form-control" required>
            </div>

            <div class="d-grid mb-3">
                <input type="submit" value="Submit" class="btn btn-primary">
            </div>
            @if($result===true) {{-- if true --}}
                <div class="alert alert-success mb-4 text-center">
                    <label class="form-control-label mb-2">{{$message}}</label>
                </div>
            @elseif($result===false) {{--if false--}}
                <div class="alert alert-danger mb-4 text-center">
                    <label class="form-control-label mb-2">An error has occurred, Message: {{$message}}</label>
                </div>
            @else
                {{-- we do nothing here. --}}
            @endif
        </form>
    </div>
@stop
