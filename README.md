## About PXL Data Import

Dificulties:
	Master branch does not work 100%, ran into a couple of issues, such as:
	 - Full json file upload times out in browser, and that is related to the server time out set.
	 - When running migrations to create tables I had to update DB_HOST var in .env file to 'localhost',
		then when completed running the migrations, I then switch DB_HOST back to 'db', and everything works.

## My Experience

When I received this task, it looked extremely complicated, but figured its the best time to test myself.
It was fun!!, I learned alot, and definately getting my hands more dirty with Laravel after this :)