$(function () {
    $(document).ready(function () {
        $('#fileUploadForm').ajaxForm({
            beforeSend: function () {
                var percentage = '0';
            },
            uploadProgress: function (event, position, total, percentComplete) {
                var percentage = percentComplete;
                $('.progress .progress-bar').css("width", percentage+'%', function() {
                    return $(this).attr("aria-valuenow", percentage) + "%";
                })
            },
            complete: function (xhr) {
                console.log('File has uploaded');
            }
        });
    });
});
